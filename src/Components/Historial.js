import React from "react";
import Card from "react-bootstrap/Card";

const Historial = props => {
  return (
    <div className="mx-2">
      <Card border="success">
        <Card.Header className="text-center">
          Scrum {props.historial.ganador}
        </Card.Header>
        <Card.Body>
          <Card.Text>Fecha de inicio: {props.historial.fechai}</Card.Text>
          <Card.Text>Fecha de finalizacion: {props.historial.fechaf}</Card.Text>
        </Card.Body>
      </Card>
      <br />
    </div>
  );
};

export default Historial;
