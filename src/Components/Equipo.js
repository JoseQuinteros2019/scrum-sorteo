import React from "react";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

const Equipo = (props) => {
 let handleSubmit = (e) => {
    
   e.preventDefault();
   let datos = e.target.integrante.value;
   console.log(datos);
   props.guardar(datos);
   e.target.integrante.value= "";
     }


  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <Form.Group >
          <Form.Label>Ingrese el integrante del equipo</Form.Label>
          <Form.Control type="text" name="integrante" placeholder="ingrese el integrante del equipo" />
         
        </Form.Group>

        
        
        <Button variant="primary" type="submit">
          Ingresar
        </Button>
      </Form>
    </div>
  );
};

export default Equipo;
