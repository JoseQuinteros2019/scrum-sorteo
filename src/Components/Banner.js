import React from 'react';
import banner from '../banner.jpg';

const Banner = () => {
    return (
        <div>
            <img src={banner} className="img-fluid" alt="imagen"></img>
        </div>
    );
};

export default Banner;