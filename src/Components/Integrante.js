import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import Button from "react-bootstrap/Button";


const Integrante = (props) => {
    return (
        <div className="row">
            <div className="col-lg-12 col-sm-12">
                <ListGroup.Item>{props.integrantes}
                    <div class="button-container">
                    <Button onClick={() => {
                    props.handleDelete(props.id)
                }} >
                        <FontAwesomeIcon icon={faWindowClose} pull="right" />
                        </Button>
                        </div>
                </ListGroup.Item>
          
          </div>
            
        </div>
    );
};

export default Integrante;