import React from "react";
import ListGroup from "react-bootstrap/ListGroup";
import Integrante from "./Integrante";

const ListaIntegrantes = props => {
  return (
    <div className="my-3">
      <ListGroup>
        {props.integrantes.map((integrantes, id) => (
          <Integrante
            key={id}
            integrantes={integrantes}
            id={id}
            handleDelete={props.handleDelete}
          >
            {" "}
          </Integrante>
        ))}
      </ListGroup>
    </div>
  );
};

export default ListaIntegrantes;
