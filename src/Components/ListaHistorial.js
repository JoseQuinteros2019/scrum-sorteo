import React from "react";
import CardGroup from "react-bootstrap/CardGroup";
import Historial from "./Historial";

const ListaHistorial = props => {
  return (
    <div>
      <h2> Historial</h2>

      <CardGroup>
        {props.historial.map((historial, id) => (
          <Historial key={id} historial={historial} id={id}>
            {" "}
          </Historial>
        ))}
      </CardGroup>
    </div>
  );
};

export default ListaHistorial;
