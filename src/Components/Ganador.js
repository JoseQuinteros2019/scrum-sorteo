import React from "react";
import Alert from "react-bootstrap/Alert";
const Ganador = props => {
  return (
    <div>
      <Alert variant="success my-2">El Scrum es {props.ganador}</Alert>
    </div>
  );
};

export default Ganador;
