import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

const Sorteo = props => {
  let handleSubmit = e => {
    e.preventDefault();
    let fechainicio = e.target.fechainicio.value;
    let fechafinal = e.target.fechafinal.value;
    //console.log(datos);
    props.sorteo(fechainicio, fechafinal);
  };

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-lg-6">
            <Form.Group>
              <Form.Label>Fecha de Inicio</Form.Label>
              <Form.Control
                type="date"
                name="fechainicio"
                placeholder="ingrese el integrante del equipo"
              />
            </Form.Group>
          </div>
          <div className="col-lg-6">
            <Form.Group>
              <Form.Label>Fecha de Finalizacion</Form.Label>
              <Form.Control
                type="date"
                name="fechafinal"
                placeholder="ingrese el integrante del equipo"
              />
            </Form.Group>
          </div>
        </div>

        <Button variant="success" type="submit">
          Sortear
        </Button>
      </Form>
    </div>
  );
};

export default Sorteo;
