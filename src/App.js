import React, { Component } from "react";
import Equipo from "./Components/Equipo";
import ListaIntegrantes from "./Components/ListaIntegrantes";

import Sorteo from "./Components/Sorteo";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

import ListaHistorial from "./Components/ListaHistorial";
import Ganador from "./Components/Ganador";
import Banner from "./Components/Banner";

class App extends Component {
  state = {
    integrante: "",
    integrantes: [],
    scrum: "",
    historial: [],
    fechainicio: "",
    fechafinal: ""
  };

  componentDidMount = () => {
    let datosLocalStorage = JSON.parse(localStorage.getItem("sorteoKey"));

    if (datosLocalStorage) {
      this.setState({
        historial: datosLocalStorage
      });
    }
  };

  componentDidUpdate = () => {
    localStorage.setItem("sorteoKey", JSON.stringify(this.state.historial));
  };

  guardarIntegrante = integrante => {
    let arregloProvisorio = this.state.integrantes;
    arregloProvisorio.push(integrante);
    this.setState({
      integrantes: arregloProvisorio
    });
  };
  Sorteo = (fechai, fechaf) => {
    let arregloProvisoriofi = fechai;
    let arregloProvisorioff = fechaf;
    let arreglo = this.state.integrantes.length;
    let seleccion = Math.floor(Math.random() * (arreglo - 0)) + 0;
    let ganador = this.state.integrantes[seleccion];
    let arreglohistorial_ = this.state.historial;
    let historia = { fechai, fechaf, ganador };

    console.log(arreglo);
    console.log(seleccion);
    historia.fechai = arregloProvisoriofi;
    historia.fechaf = arregloProvisorioff;

    historia.ganador = ganador;

    arreglohistorial_.push(historia);
    this.setState({
      fechainicio: arregloProvisoriofi,
      fechafinal: arregloProvisorioff,
      scrum: ganador,
      historial: arreglohistorial_
    });
  };
  handleDelete = id => {
    console.log(id);
    let arreglo = this.state.integrantes;
    arreglo.splice(id, 1);

    this.setState({
      integrantes: arreglo
    });
  };
  render() {
    return (
      <div className="container">
        <div className="text-center">
          <Banner />
        </div>

        <div className="row">
          <div className="col-lg-12 text-center  ">
            <Equipo guardar={this.guardarIntegrante} />
          </div>

          <div className="col-lg-6 col-sm-12 ">
            <ListaIntegrantes
              integrantes={this.state.integrantes}
              handleDelete={this.handleDelete}
            />
          </div>
          <div className="col-lg-6 col-sm-12 my-1">
            <Sorteo sorteo={this.Sorteo} />
            {this.state.scrum !== "" ? (
              <Ganador ganador={this.state.scrum} />
            ) : null}
          </div>

          {this.state.historial.length > 0 ? (
            <ListaHistorial historial={this.state.historial} />
          ) : null}
        </div>
      </div>
    );
  }
}

export default App;
